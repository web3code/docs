# Documentation

## Smart Contract

### `ConstantPublic.sol`

```mermaid
classDiagram
    class ConstantPublic {
        constant SetHeroDataMask_Base = 0x1;
        constant SetHeroDataMask_Gene = 0x2;
        constant SetHeroDataMask_Growth = 0x4;
    }
```

### `HeroErc721A.sol`

```mermaid
classDiagram
    class HeroErc721 {

        bytes32 public constant OWEN_ROLE 
        bytes32 public constant UPGRADER_ROLE
        bytes32 public constant OPERATOR_ROLE
        address heroWrapContractAddress;
        private mapHeroBase
        private mapHeroGene
        private mapHeroGrowth
        string private strBaseURI

        function initialize()
        function SetBaseURI(string memory strURI)
        function pause()
        function unpause()
        function safeTransferFrom(address from, address to, uint256 tokenId)
        function SetHeroDataEx(uint256 [] memory tokens, uint128 [] memory datas, uint8 [] memory uMasks)
        function Burns(uint256 [] calldata tokens)
        function GetHeroData(uint256 token)
        function GetHeroDatas(uint256[] memory tokens)
        function TokensOwnersOf(uint256[] calldata tokens, address owner)
        function ownerOfs(uint256[] memory tokens)
        function safeMintFromHeroWrapEx(MintOrder [] memory orders)
        
    }

    class MintOrder {
        uint uOrderId;
        address userAddress;
        HeroData [] heroDatas;
    }

    class HeroData {
        HeroBase base;
        uint128 genes;
        uint128 growth;
    }
```

### `HeroGenesConfigData.sol`

```mermaid
classDiagram
    HeroGenesHelper <|-- HeroGenesConfigData
    class HeroGenesHelper {
        uint16 [36] public BitCursor
        uint16 public constant GENES_FRAGMENT_GENERATE_COLUMN = 35
        uint32 constant allOne32
        uint128 constant allOne128
        function SetValueByBits128(uint128 orgValue, uint128 setValue, uint256 maskLeft, uint256 maskLength)
        function GetValueByBits128(uint128 value, uint256 maskLeft, uint256 maskLength) public pure returns(uint128 outValue)
    }
    class HeroGenesConfigData {
        function CanForge(HeroErc721.HeroData memory hostHero, HeroErc721.HeroData memory burnHero, EnumGenesBit emGeneBit)
        function SetForgeGenes(HeroErc721.HeroData [] memory heroDatas,uint128[] memory uValues,HeroGenesConfigData.EnumGenesBit [] memory emGeneBits) 
    }

```

### `HeroWrap.sol`

```mermaid
classDiagram
    class HeroWrap {
        bytes32 public constant OWEN_ROLE
        bytes32 public constant UPGRADER_ROLE
        bytes32 public constant OPERATOR_ROLE
        HeroErc721 heroErc721Contract;
        IERC20 aErc20;
        MysteryBoxWrap mysteryBoxWrap;
        ItemErc1155 itemErc1155;
        address public financeAddress;
        function SetFinanceAddress(address add)
        
        function initialize()
        function SetBitCursorSetting(uint16 [36] calldata setting)
        function ForgeEx(uint256 tokenHost, uint256 tokenBurn, EnumGenesBit emGeneBit)
        function HeroCharge(uint256 [] memory tokensBurn)
        function _checkTokensOwner(uint256 tokenHost, uint256 [] memory tokensBurn)
        function SetPowerGrowUpCost(uint [4] calldata setting)
        function SetForgeCost(uint [4] calldata setting)
        function GetNewOrderId()
        function ConsumeOrderIds(uint [] memory orderIds)
        function OpenMysteryBoxesEx(uint id, uint amount, address to)
        function safeMintFromOracle(HeroErc721.MintOrder [] memory orders)
        function SetOracleAddress(address add)
        function ForgeFromOracle(uint256 [] calldata tokens, EnumGenesBit [] calldata emGeneBits, uint128 [] memory uGeneValues,uint [] calldata uOrderIds)
        function GrowUpEx(bool bPowerUp, uint256 tokenHost, uint256 [] memory tokensBurn)
        function _checkHeroRare(HeroErc721.HeroData [] memory heroDatas)
        function GrowUpFromOracle(uint256 [] calldata tokens,uint128 [] calldata hostsData,uint8 [] calldata uMasks, uint [] calldata uOrderIds)
    }

```

### `ItemErc1155.sol`

```mermaid
classDiagram
    class ItemErc1155 {
        bytes32 public constant URI_SETTER_ROLE
        bytes32 public constant PAUSER_ROLE
        bytes32 public constant MINTER_ROLE
        bytes32 public constant UPGRADER_ROLE
        bytes32 public constant OPERATOR_ROLE
        function initialize()
        function setURI(string memory newuri)
        function pause()
        function unpause()
        function _beforeTokenTransfer(address operator, address from, address to, uint256[] memory ids, uint256[] memory amounts, bytes memory data)
        function _beforeTokenTransfer(address operator, address from, address to, uint256[] memory ids, uint256[] memory amounts, bytes memory data)
        function checkBurnAccessRight(uint256[] memory ids)
        function SetItemTypeAccessRight(uint uItemType, address addressContract, bool[2] calldata accessRightMintAndBurn)
        function MintEx(address to, uint256[] memory ids, uint256[] memory amounts)
        function MintMultiAddressEx(address[] memory tos, uint256[] memory ids, uint256[] memory amounts)
        function BurnEx(address account, uint256[] memory ids, uint256[] memory amounts)
    }

```

### `MerkleTreeWhitelist.sol`

```mermaid
classDiagram
    class MerkleTreeWhitelist {
        public whiteLists;
        public claimed
        bytes32 public constant OPERATOR_ROLE
        bytes32 public constant UPGRADER_ROLE
        address public mysteryBoxWrap;
        address public arcaToken;
        function initialize()
        function setWhiteList(string memory name,bytes32 merkleRoot, uint[][] memory giftList,uint arca,uint startTime,uint endTime)
        function getWhiteList(string memory name)
        function deleteWhiteList(string memory name)
        function claim(string memory whitelistName, bytes32[] memory merkleProof)        
    }

    class WhiteList {
    bytes32 merkleRoot;
    uint[][] giftList;
    uint arca;
    uint startTime;
    uint endTime;
  }

```

### `MysteryBoxWrap.sol`

```mermaid
classDiagram
    class MysteryBoxWrap {
        bytes32 public constant PAUSER_ROLE
        bytes32 public constant UPGRADER_ROLE
        bytes32 public constant OPERATOR_ROLE
        address financeAddress
        public mapMysteryBox
        public amountSupply
        public addressAmountHasMint
        ItemErc1155 itemErc1155
        HeroWrap heroWrap
        VoucherWrap voucherWrap
        IERC20 aErc20

        function SetCashierAddress(address add)
        function SetFinanceAddress(address add)
        function pause()
        function unpause()
        function _authorizeUpgrade()
        function Buy(currencyEnum emCurrency, uint id, uint amount)
        function Open(uint id, uint amount)
        function Airdrop(address[] memory toAddresses, uint256 [] memory ids, uint256 [] memory amounts)
        function DealwithAmount(address userAddress, uint id, uint amount, bool bCheckTime)
        function SetBox(MysteryBox calldata box)
        function RemoveBox(uint id)
        function GetBoxInfo(uint id)
        function GetAmountsSupply(uint [] calldata ids)
        function ItemIndex2MysteryBoxId(uint uItemIndex)
        function MysteryBoxFromGame(MysteryTicket memory ticket, bytes memory signature)
        function claimFromMerkleTreeWhitelist(address account, uint[][] calldata giftList)
        ExchangeVoucher(address userAddress, uint idVoucher, uint amountVoucher, uint[] memory exchangeToIds, uint[] memory exchangeToAmounts)
    }

    class MysteryBox {
        
        // ID
        uint id;
        
        // 开始时间 0表示没有开始时间
        uint timeStart;

        // 结束时间 0表示没有结束时间
        uint timeEnd; 

        // Total supply
        uint totalSupply;

        // Allow amount on one address.
        uint allowAmountPerAddress;

        // 价格列表
        uint [] prices;        
    }

    class MysteryTicket {
        address fromCashier; // 签名
        address user;       // 用户地址
        uint256 uItemIndex;      // 道具ID        
        uint16 uNumber;        // 数量
        uint64 salt;       // salt 我们作为订单
        uint64 expireAt;   // 过期时间
    }

```

### `VoucherWrap.sol`

```mermaid
classDiagram
    class VoucherWrap {
        bytes32 public constant OPERATOR_ROLE
        bytes32 public constant UPGRADER_ROLE
        function initialize()
        function _authorizeUpgrade(address newImplementation)
        function pause()
        function unpause()
        function SetVoucher(uint id, uint[] memory exchangeToIds, uint[] memory exchangeToAmounts, uint exchangeTimeStart, uint exchangeTimeEnd)
        function GetVoucher(uint id)
        function Exchange(uint id, uint amount)
    }
    class VoucherInfo {        
        uint[] exchangeToIds;      // 兑换到的物品id列表，如果长度为0，表示无法兑换。
        uint[] exchangeToAmounts;   // 兑换物品数量，长度需要和ids匹配
        uint exchangeTimeStart; // 兑换开始时间
        uint exchangeTimeEnd;   // 兑换结束时间
    }
    
```

## E2E Test

1. hardhat for automation and merkletree for proof
2. hero data mask has three types and identified by integer number
3. hero has base, gene and growth data
4. contract initialization steps: breakdown
5. erc1155 item has three types: 0, 1, 2
    1. 0 -> mystery box
    2. to be determined
6. erc1155 item access rights are configurable.
7. vouncher airdrop Test
8. mystery box from game needs valid signature and signature from cachier
9. buy mystery box
10. airdrop mystery box
11. open mystery box
12. mint hero from mint order by oracle
13. relationship between mystery box and voucher is settable
14. exchange voucher with mystery box
15. use merkletree to generate proof
16. claim by address in the white list
17. burn hero token and forge host hero token
18. cachier has right to call oracle function
19. burn hero and grow up host hero token
20. charge hero list with hero list


## Question
